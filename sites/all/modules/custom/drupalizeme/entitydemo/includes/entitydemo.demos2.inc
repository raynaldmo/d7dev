<?php

function entitydemo_page2() {
  $query = new EntityFieldQuery();

  // Get all node entity types
  $query->entityCondition('entity_type', 'node');
  $result = $query->execute();
  dsm($result, 'result');

  // Get all entities that have a field called field_number and value is > 100
  // $query = new EntityFieldQuery();
  // $query->fieldCondition('field_number', 'value', 100, '>');
  // dsm($result);

  // Get all node entities with specified promote property
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('promote', 1)
    ->execute();

  dsm($result, 'result');

  // Load nodes from above query
  $nids = array_keys($result['node']);
  dsm($nids, 'keys');
  $nodes = entity_load('node', $nids);
  dsm($nodes, 'nodes');


  return 'This is a demo page for demonstrating EntityFieldQuery API';
}