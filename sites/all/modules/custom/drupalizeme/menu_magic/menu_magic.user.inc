<?php

/**
 * Page callback for the 'user/%/magic tab.
 */
function menu_magic_user_tab($wildcard) {
  if (is_numeric($wildcard) && ($account = user_load($wildcard))) {
    return array(
      '#type'   => 'markup',
      '#markup' => t("%username is totally awesome.", array('%username' => $account->name)),
    );
  }
  drupal_not_found(); // send '404' page

  return array();
}