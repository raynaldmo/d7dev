<?php

function menu_magic_extra($wildcard) {
  // renderable array - Drupal likes for content output to be specified
  // in this fashion!
  $content = array(
    '#type' => 'markup',
    '#markup' => '<p>'. t('The wildcard contains the value "%wildcard".', array('%wildcard' => $wildcard)) . '</p>',
  );
  return $content;
}