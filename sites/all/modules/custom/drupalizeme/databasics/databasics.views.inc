<?php

/**
 * Implements hook_views_data
 */
function databasics_views_data() {
  $data = array();

  // Define group for databasics table fields
  $data['databasics']['table']['group'] = t('Databasics');

  $data['databasics']['table']['base'] = array(
    // Info for views UI
    'title' => t('Databasics'),
    'help' => t('Stores node visits for users.'),
  );

  // Define tables we can JOIN to databasics table
  $data['databasics']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',    // node table field
      'field' => 'nid'         // databasics table field
    ),
    'users' => array(
      'left_field' => 'uid',    // user table field
      'field' => 'uid'         // databasics table field
    ),
  );

  // Describe fields in databasics db table

  // Node NID field
  $data['databasics']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The node ID.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Information for joining node table
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    )
  );

  // User UID field
  $data['databasics']['uid'] = array(
    'title' => t('User Id'),
    'help' => t('The user ID of the user that viewed the node'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      // 'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      // 'handler' => 'views_handler_sort',
      'handler' => 'databasics_handler_sort_custom',
    ),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Visitor'),
    ),
  );

  // View count field
  $data['databasics']['view_count'] = array(
    'title' => t('Visits'),
    'help' => t('The number of times the node has been viewed'),

    // Describe handlers
    'field' => array(
      'handler' => 'databasics_handler_field_percent',
      'click sortable' => TRUE,
    ),
    // ORDER BY clause in our SQL builder
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    // Filter handler
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      //  For some reason this handler doesn't work.
      // See file databasics_handler_filter_numeric.inc
      // 'handler' => 'databasics_handler_filter_numeric',
    ),
  );


  // View count percent 'pseudo' field.
  // This field is NOT present in databasic db table but it'll be available
  // to views by virtue of it being defined here
  $data['databasics']['view_count_percent'] = array(
    'title' => t('Visits graph'),
    'help' => t('The number of times the node has been viewed displayed as a bar graph'),
    'real field' => 'view_count',

    // Describe handlers
    'field' => array(
      'handler' => 'databasics_handler_field_percent_graph',
      'click sortable' => TRUE,
    ),
  );

  // Last visited field
  $data['databasics']['last_viewed'] = array(
    'title' => t('Last visit'),
    'help' => t('Timestamp of when the page was visited.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),

  );

  // 'Area' handler
  // Tells Views what can display in header, footer and empty text in a view
  $data['databasics']['databasics_area'] = array(
    'title' => t('Databasics Area'),
    'help' => t('Display a summary of total views'),
    'area' => array(
      'handler' => 'databasics_handler_area_databasics'
    ),
  );

  return $data;

}