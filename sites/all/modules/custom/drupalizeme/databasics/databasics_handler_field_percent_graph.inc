<?php

// This code doesn't work quite right as when the view is configured to show
// "Visits graph" field only the Visits percentage is also displayed

class databasics_handler_field_percent_graph extends databasics_handler_field_percent {
  function render($values) {
    $value = $this->get_value($values);
    return '<div class="databasics-graph" style="display:block; background: #5aaeff; height: 10px; width: ' . $value . '%;">' . $value . '</div>';
  }
}