<?php

class databasics_handler_field_percent extends views_handler_field_numeric {

  /**
   * databasics_handler_field_percent constructor.
   */
  public function __construct() {
    parent::construct();

    // This makes sure node ID is always available when we call our
    // pre-render method even if node ID isn't part of the view
    $this->additional_fields['nid'] = 'nid';

    // Tell views API to treat field as a float value
    // This setting should have given us 'Round' and 'Precision' options
    // when we configure the view_count field but it doesn't seem to work.
    // We'll just use php round() in the pre_render method to achieve the
    // same effect.
    $this->definition['float'] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Add custom checkbox. Checking it will display view count as percentage
    // We're just using the standard Form API render array below
    $form['percent'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display as percent'),
      '#description' => t('If checked display the number as percent of total views of this node by all users.'),
      '#default_value' => $this->options['percent'],
    );
  }

  /**
   * {@inheritdoc}
   */
  function query() {
    parent::query();
    if ($this->options['percent']) {
      $this->add_additional_fields();
    }
  }

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();

    // Set percent option to disabled by default
    $options['percent'] = array('default' => FALSE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function pre_render(&$values) {
    parent::pre_render($values);
    // $values is an array holding rows in databasics db table
    // dsm($values);

    // Custom code to render view count as percentage
    if ($this->options['percent']) {
     foreach($values as $key => $row) {
       // Below line doesn't work - there is no aliases array
       // $nid = $row->{$this->aliases['nid']};
       // Below line works but may not be robust
       // $nid = $row->node_databasics_nid;
       $nid = $row->databasics_nid;
       $total = db_query('SELECT SUM(view_count) FROM {databasics} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
       $count = $this->get_value($row);
       $percentage = ($count / $total) * 100;
       $values[$key]->{$this->field_alias} = round($percentage,1);
     }
    }
  }
}