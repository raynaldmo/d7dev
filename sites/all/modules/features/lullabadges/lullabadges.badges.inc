<?php
/**
 * @file
 * lullabadges.badges.inc
 */

/**
 * Implements hook_badges_info().
 */
function lullabadges_badges_info() {
  $export = array();

  $badge = new stdClass();
  $badge->disabled = FALSE; /* Edit this to true to make a default badge disabled initially */
  $badge->api_version = 1;
  $badge->name = 'testbadge_1';
  $badge->text = 'Test badge #1';
  $badge->color = 'ff6699';
  $export['testbadge_1'] = $badge;

  $badge = new stdClass();
  $badge->disabled = FALSE; /* Edit this to true to make a default badge disabled initially */
  $badge->api_version = 1;
  $badge->name = 'testbadge_2';
  $badge->text = 'Test badge #2';
  $badge->color = '66ffff';
  $export['testbadge_2'] = $badge;

  $badge = new stdClass();
  $badge->disabled = FALSE; /* Edit this to true to make a default badge disabled initially */
  $badge->api_version = 1;
  $badge->name = 'testbadge_3';
  $badge->text = 'Test badge #3';
  $badge->color = 'ffcc99';
  $export['testbadge_3'] = $badge;

  return $export;
}
