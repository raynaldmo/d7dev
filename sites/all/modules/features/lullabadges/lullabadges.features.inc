<?php
/**
 * @file
 * lullabadges.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function lullabadges_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "badges" && $api == "badges") {
    return array("version" => "1");
  }
}
