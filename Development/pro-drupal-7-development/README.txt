Exercises and sample code from text "Pro Drupal 7 Development"
===============================================================
ch02
--------
- Build annotate module
- Create annotation settings page @ admin/config/annotate/settings

APIs:
hook_install()
  field_info_field()
  field_create_field()
hook_uninstall()
  node_type_get_types()
  field_info_field()
  field_delete_field()
hook_help()
hook_menu()
  system_admin_menu_block_page() (page callback)

  drupal_get_form() (page callback)
    annotate_admin_settings
      system_settings_form

hook_node_load()
hook_trigger_info()

ch03
-----
- Build beep module

- How do I call module_invoke_all() so that ANNOTATE tab appears on
Triggers config screen ?

A: After disabling/enabling module, ANNOTATE tab appears on Triggers
config screen. Didn't need to call module_invoke_all()

ch04
------
- Add Menu Fun menu
- Page and page callback example
- Add "Menu Fun" permissions setting to admin/people/permissions page

APIs:
hook_menu()
  user_access (access callback)
hook_permission()

ch07
------
- Working with nodes
- Often the term content type is used as a synonym for node type

ch09
-----
- Theme layer
- Installed these themes
http://ftp.drupal.org/files/projects/adaptivetheme-7.x-3.2.zip
http://ftp.drupal.org/files/projects/pixture_reloaded-7.x-3.0.zip
- Built grayscale theme
- Added breadcrumb.tpl.php template

ch10
-----
- Working with Blocks
- Build Approval module
- Created "Pending Comments" and "Unpublished Nodes" blocks

APIs:
hook_block_info()
hook_block_configure()
hook_block_save()
hook_block_view()
  db_query()
(use render array for output)

ch11
------
- The Form API
- Build formexample module
- Example of theming a form

APIs:
hook_menu()
  drupal_get_form (page callback)
    formexample_nameform
      (uses render arrays for output)
hook_theme()

ch18
------
- Using jQuery
- Enable PHP Filter module
- Create "Testing jQuery" page to experiment add jQuery directly
to a page
- Added file themes/bartik/logofade.js and added to bartik.info
to fade out and fade in Drupal logo for each page

- Build blockaway module
APIs:
hook_init()
  drupal_add_js()

- Build plusone module
- Add voting widget to each article page and display votes
for the article

APIs:
hook_schema()


