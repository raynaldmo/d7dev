8/26/15

- Exercises and sample code from drupalize.me (Drupal 7)
"Module Development for Drupal 7" course

- Installed drush following instructions in "Drush User's Guide"

Install new site
----------------
1. [raynald@localhost drupal-module-dev]$ drush dl drupal

2. create database using mysql command line

3. Install site

raynald@raynald-Inspiron-530s:/var/www/drupal-module-dev$ pwd
/var/www/drupal-module-dev


raynald@raynald-Inspiron-530s:/var/www/drupal-module-dev$

drush si --db-url=mysql://test:test@192.168.0.252/drupal_module_dev \
--site-name=drupal-module-dev standard


ch05
-----------------
- devel_example.module

ch06
-----
- Built demo module
- Add Demo module permissions to admin/people/permissions page
- Add demo_field (markup) and demo_list to every node (content)

APIs:
hook_permission()
hook_node_view()
  returns content as render array - this content appears on each
  page

ch08
-----
- Built menu_magic module
- Added "A little magic" to Navigation menu

ch09
-----
- Add to menu_magic module
- Add "Magic" tab to url <site>/drupal-module-dev/user/% (user profile info)

ch10
-----
- Add contextual 'Magic' tab to all nodes
- Use menu autoloader feature

ch11
-----
- Add to menu_magic module
- Use http://placehold.it/ or placekitten.com for 'lorem ipsum' of images
- Add image and un-ordered list to "A little magic" page

APIs:



ch12
-----
- Add css file to image in "A little magic page"
- Add caching for un-ordered list in "A little magic" page using #cache
property

ch13
------
- Added menu_mangler module to demonstrate how to alter a page
rendered by another module

ch14
------
- Spy Glass module
- Example of how to use hook_theme
- Write new (custom) theme function (theme_spy_glass_item)
- Modules should always use theme functions to output HTML/CSS to enable
theme designers to override if desired
- Demonstrate how to use theme template file vs theme function

ch15
-----
- Form API
- Explanation of form handling
- Form API is basically an extension of Render API

ch16
------
- Build Form Fun module

ch17
-----
- Continue with Form Fun module
- Use of #tree property (add file form_fun.tree.inc)

ch18
-----
- Continue with Form Fun module
- Use of #state property to create dynamic form (add file form_fun.states.inc)

ch19
-----
- Build form_demo module
- Use hook_form_alter to change password description on My account form

ch20
-----
- Continue with form_demo_module
- Use  hook_form_FORM_ID_alter to alter user registration page

ch21
-----
- Continue with form_demo_module (admin/config/people/passwords)
- Add admin page/form to set bad passwords
- Use system_settings_form() to save bad passwords to db

ch23
------
- Build db_demo module.
- Add page @ admin/reports/node-list to show list of published and
unpublished node types
- use render() function to output item-list

APIs:
hook_menu()
  db_demo_node_list (page callback)
    node_type_get_types()
    db_select()

ch24
------
- Build db_table_demo module (Example of adding new table to database)
- For each node type (article, basic page etc.) display message
"You have viewed this page x times"
- Use Schema module to convert database table to hook_schema array
- Need to run update.php to update/add table to database
- Created new table db_table_demo in drupal-module-dev database

APIs:
hook_schema()


ch25
------
- Continue with db_table_demo module
- Add ability to read and save view count from db_demo_table table


Database cheat-sheet
---------------------
http://drupal.org/developing/api/database

SELECT QUERY

  //SELECT column1, column2, columnN
  //FROM {tablename}
  //WHERE column1 = $value1
  //AND column2 <> $value2
  //ORDERBY 'sticky', DESC
  //ORDERBY 'created', DESC
  //LIMIT 10;

  $query = db_select('tablename', 'optional alias')
    ->fields('tablename or alias if given', array('colunm1', 'column2' ..., 'columnN'))
    ->condition('column1', 'value1')
    ->condition('column2', 'value2', 'optional <>')
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC')
    ->limit(variable_get('default_nodes_main', 10))
    ->extend('PagerDefault');
  return $query->execute()->fetch();
  //Calling fields() with no field list will result in a "SELECT *" query.



INSERT QUERY

  //INSERT INTO {tablename} ('column1', 'column2') VALUES ('value1', 'value2');

  $query = db_insert('tablename')
    ->fields(array('column1', 'column2'))
    ->values(array('column1' => 'value1', 'column2' => 'value2'))
    ->execute();



UPDATE QUERY

  //UPDATE {tablename}
  //SET column1 = value1, column2 = value2
  //WHERE column3 >= value3

  $query = db_update('tablename')
    ->fields(array('column1' => 'value1', 'column2' => 'value2'))
    ->condition('column3', 'value3', '>=')
    ->execute();
  //The third parameter for ->condition() is optional and defaults to "="



MERGE QUERY

  //In tablename, if the field = value exists, update field1 = value1 && field2 = value2
  //UPDATE {tablename} SET column1 = value1, column2 = value2 WHERE column = value

  //If tablename, if the field = value does NOT exists,
  //INSERT INTO {tablename} ('column', 'column1', 'column2') VALUES ('value', 'value1', 'value2');

  $query = db_merge('tablename')
    ->key(array('column' => 'value'))
    ->fields(array(
        'column1' => 'value1',
        'column2' => 'value2',
    ))
    ->execute();



DELETE QUERY
  //DELETE FROM {tablename} WHERE column = value;
  $query = db_delete('tablename')
    ->condition('column', 'value')
    ->execute();

ch26
-----
- Continue with db_table_demo module
- Add new database field last_viewed
- Run update.php to update db_table_demo table


DONE!