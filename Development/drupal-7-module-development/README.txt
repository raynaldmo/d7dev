8/25/15

Exercises and sample code from text "Drupal 7 Module Development"
===============================================================
ch02
-----
- Build first module (list all enabled modules)

APIs:
hook_help()
hook_block_info()
hook_block_view()
  module_list()

ch03
-----
- No modules
- Drupal calls the process of wrapping your data in HTML and CSS as theming.
- In addition to API hooks, Drupal also has theme hooks.
- A theme hook is simply the name of a particular way to markup some data
- Use of theme() function
- There are actually two different ways to implement a theme hook:

1) Theme functions: pass data to a PHP function to wrap it in markup

theme_item_list
theme_form etc.

// Example
$variables = array('items' => $list, 'type' => 'ol');
$content = theme('item_list', $variables);

2) Templates: pass data to a template which is a PHP file mixed with markup
and PHP print statements

- Render elements
Render elements are new to Drupal 7's theme layer.
They've existed since Drupal 4.7 as part of the Form API, but they've now been
injected into the heart of the theme system.

- A render element is a complex data structure passed as a single parameter to
theme(), as one of its variables.

ch04
-----
- Build single_blog module

- In Drupal 7, all hook_block_view() and all page callbacks
(the functions that return the main contents of a page) should return a
renderable array instead of a string.

APIs:
hook_help()
hook_block_info()
hook_block_view()
  user_access()
  db_select()
hook_theme()
  (- Example use of render array and template file)
hook_menu()
  single_blog_archive_page()
    db_query()
    node_load()
    node_view()

ch05
------
- Build user_warn module
- Create /admin/config/people/user_warn page to set warning subject line and body
- Create "Warn" tab on user profile page

APIs:
hook_help()
hook_menu()
  drupal_get_form (page callback)
    user_warn_form()
      user_warn_form_submit()
        variable_set()
  drupal_get_form (page callback)
        user_warn_confirm_form()
          user_warn_confirm_form_submit()
            drupal_mail()
hook_mail()